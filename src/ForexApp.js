import React from 'react';
import Forex from './forexComponents/Forex';

function ForexApp () {
        return(
            <div
                className='bg-light d-flex justify-content-center align-items-center min-vh-100'
            >
                <Forex />
            </div>
        )
}

export default ForexApp;