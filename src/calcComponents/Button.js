import React from 'react';

function Button (props) {
    return(
        <button 
            className= { props.color } 
            onClick = { props.handleOnClick }
        >
            {props.text} 
        </button>
    )
}

export default Button;