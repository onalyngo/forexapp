import React from 'react';
import Button from './Button';

function Buttons (props) {
        return (
            <div 
                className='d-flex justify-content-around'
            >
            <Button 
                color = {'btn-info'}
                text = {'+'}
                //Step 3: Pass the function we received as a property
                //to Button Component
                handleOnClick = { props.handleAddition }
            />
            <Button 
                color = {'btn-info'}
                text = {'-'}
                handleOnClick = { props.handleSubtraction }
            />
            <Button 
                color = {'btn-info'}
                text = {'x'}
                handleOnClick = { props.handleMultiplication }
            />
            <Button 
                color = {'btn-info'}
                text = {'/'}
                handleOnClick = { props.handleDivision }
            />
            <Button 
                color = {'btn-info'}
                text = {'Clear'}
                handleOnClick = { props.handleClear }
            />
            </div>
        )
}

export default Buttons;