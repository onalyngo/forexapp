import React from 'react';
import { FormGroup, Label, Input} from 'reactstrap';

function CalcInput (props){
    return(
        <FormGroup>
                <Input 
                    placeholder = { props.placeholder }
                    defaultValue = { props.defaultValue }
                    onChange = { props.onChange }
                    type = 'number'
                />
        </FormGroup>
    )
}

export default CalcInput;