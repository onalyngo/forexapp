import React, { useState } from 'react';
import CalcInput from './CalcInput';
import Buttons from './Buttons';
import { Button } from 'reactstrap';

const Calculator = () =>{

    const [inputNumber1, setInputNumber1] = useState (0);
    const [inputNumber2, setInputNumber2] = useState (0);
    const [resultNumber, setResultNumber] = useState (0);


    const handleInputNumber1 = event => {
        setInputNumber1(event.target.value);
    }

    const handleInputNumber2 = event => {
        setInputNumber2(event.target.value);
    }

    const handleAddition = () =>{
        setResultNumber(parseInt(inputNumber1) + parseInt(inputNumber2));
    }

    const handleSubtraction = () =>{
        setResultNumber(parseInt(inputNumber1) - parseInt(inputNumber2));
    }

    const handleMultiplication = () =>{
        setResultNumber(parseInt(inputNumber1) * parseInt(inputNumber2));
    }

    const handleDivision = () =>{
        setResultNumber(parseInt(inputNumber1) / parseInt(inputNumber2));
    }

    const handleClear = () =>{
        setResultNumber(0);
    }

    return(
        <div>
            <h1 className='text-center my-5'>Simple Calculator</h1>
            <div
               className='d-flex justify-content-center flex-column'
            >
                <CalcInput
                    placeholder = { 'Input number' }
                    onChange = { handleInputNumber1 }
                />
                <Buttons 
                    handleAddition = { handleAddition }
                    handleSubtraction = { handleSubtraction }
                    handleMultiplication = { handleMultiplication }
                    handleDivision = { handleDivision }
                    handleClear = { handleClear }
                />
                <br></br>
                <CalcInput
                    placeholder = { 'Input number' }
                    onChange = { handleInputNumber2 }
                />
                {/* <Button
                    color = 'info'
                    onClick = { handleEqual }
                > */}
                {/* =  
                </Button> */}
                <div style={{marginTop: "10%", border: 'solid gray 1px'}}>
                    <div>
                        <h5 className='text-center'>
                            {resultNumber}
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Calculator;