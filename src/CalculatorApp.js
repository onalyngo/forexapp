import React from 'react';
import Calculator from './calcComponents/Calculator';

function CalculatorApp () {
    return(
        <div 
            className='bg-light d-flex justify-content-center align-items-center min-vh-100'
        >
            <Calculator />
        </div>
        
    )
}

export default CalculatorApp;